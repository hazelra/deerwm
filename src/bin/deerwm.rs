use commands::Command;
use nanoserde::DeBin;
use std::io::Read;

use deerwm::commands;

fn main() {
    let cmd_rx = commands::command_reciever(commands::DEFAULT_FIFO);

    'run: loop {
        while let Ok(cmd) = cmd_rx.try_recv() {
            match cmd {
                Command::Quit => break 'run,
                _ => (),
            }
        }
    }
}
