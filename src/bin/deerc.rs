use std::io::Write;

use deerwm::commands;
use nanoserde::SerBin;

fn main() {
    let mut cmd_fifo = unix_named_pipe::open_write(commands::DEFAULT_FIFO)
        .expect("failed to open fifo for writing");

    let quit_cmd = commands::Command::Quit;
    let mut cmd_buffer = Vec::new();
    quit_cmd.ser_bin(&mut cmd_buffer);

    cmd_fifo
        .write_all(&cmd_buffer[..])
        .expect("failed to write to fifo");
}
